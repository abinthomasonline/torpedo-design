#include <iostream>

using namespace std;

int main() {
	int n_x=4, n_y=4, n_z=4;
	double potential_l = -10;
	double potential_r = 10;

	// allocate
	double *x = new double[n_x*n_y*n_z];
	double *b = new double[n_x*n_y*n_z];
	int **A = new int*[n_x*n_y*n_z];
	#pragma omp parallel 
	{
		#pragma omp for
		for(int i=0; i<n_x*n_y*n_z; ++i) {
			A[i] = new int[n_x*n_y*n_z];
		}

		//init
		#pragma omp for collapse(6) nowait
		for(int i_z=1; i_z<n_z-1; ++i_z) {
			for(int i_y=1; i_y<n_y-1; ++i_y) {
				for(int i_x=1; i_x<n_x-1; ++i_x) {
					for(int j_z=0; j_z<n_z; ++j_z) {
						for(int j_y=0; j_y<n_y; ++j_y) {
							for(int j_x=0; j_x<n_x; ++j_x) {
								int row_n = i_z*n_x*n_y+i_y*n_x+i_x;
								int col_n = j_z*n_x*n_y+j_y*n_x+j_x;
								if(col_n==row_n) {
									A[row_n][col_n] = 6;
								} else if(col_n==((i_z+1)*n_x*n_y+i_y*n_x+i_x)) {
									A[row_n][col_n] = -1;
								} else if(col_n==((i_z-1)*n_x*n_y+i_y*n_x+i_x)) {
									A[row_n][col_n] = -1;
								} else if(col_n==(i_z*n_x*n_y+(i_y+1)*n_x+i_x)) {
									A[row_n][col_n] = -1;
								} else if(col_n==(i_z*n_x*n_y+(i_y-1)*n_x+i_x)) {
									A[row_n][col_n] = -1;
								} else if(col_n==(i_z*n_x*n_y+i_y*n_x+i_x+1)) {
									A[row_n][col_n] = -1;
								} else if(col_n==(i_z*n_x*n_y+i_y*n_x+i_x-1)) {
									A[row_n][col_n] = -1;
								} else {
									A[row_n][col_n] = 0;
								}
							}
						}
					}
				}
			}
		}
		#pragma omp for collapse(6) nowait
		for(int i_z=0; i_z<n_z; i_z+=(n_z-1)) {
			for(int i_y=0; i_y<n_y; ++i_y) {
				for(int i_x=0; i_x<n_x; ++i_x) {
					for(int j_z=0; j_z<n_z; ++j_z) {
						for(int j_y=0; j_y<n_y; ++j_y) {
							for(int j_x=0; j_x<n_x; ++j_x) {
								int row_n = i_z*n_x*n_y+i_y*n_x+i_x;
								int col_n = j_z*n_x*n_y+j_y*n_x+j_x;
								if(row_n==col_n) {
									A[row_n][col_n] = 1;
								} else {
									A[row_n][col_n] = 0;
								}
							}
						}
					}
				}
			}
		}
		#pragma omp for collapse(6) nowait
		for(int i_z=0; i_z<n_z; ++i_z) {
			for(int i_y=0; i_y<n_y; i_y+=(n_y-1)) {
				for(int i_x=0; i_x<n_x; ++i_x) {
					for(int j_z=0; j_z<n_z; ++j_z) {
						for(int j_y=0; j_y<n_y; ++j_y) {
							for(int j_x=0; j_x<n_x; ++j_x) {
								int row_n = i_z*n_x*n_y+i_y*n_x+i_x;
								int col_n = j_z*n_x*n_y+j_y*n_x+j_x;
								if(row_n==col_n) {
									A[row_n][col_n] = 1;
								} else {
									A[row_n][col_n] = 0;
								}
							}
						}
					}
				}
			}
		}
		#pragma omp for collapse(6) nowait
		for(int i_z=0; i_z<n_z; ++i_z) {
			for(int i_y=0; i_y<n_y; ++i_y) {
				for(int i_x=0; i_x<n_x; i_x+=(n_x-1)) {
					for(int j_z=0; j_z<n_z; ++j_z) {
						for(int j_y=0; j_y<n_y; ++j_y) {
							for(int j_x=0; j_x<n_x; ++j_x) {
								int row_n = i_z*n_x*n_y+i_y*n_x+i_x;
								int col_n = j_z*n_x*n_y+j_y*n_x+j_x;
								if(row_n==col_n) {
									A[row_n][col_n] = 1;
								} else {
									A[row_n][col_n] = 0;
								}
							}
						}
					}
				}
			}
		}
		#pragma omp for collapse(3) nowait
		for(int i_z=1; i_z<n_z-1; ++i_z) {
			for(int i_y=1; i_y<n_y-1; ++i_y) {
				for(int i_x=1; i_x<n_x-1; ++i_x) {
					int index = i_z*n_x*n_y+i_y*n_x+i_x;
					x[index] = 0;
					b[index] = 0;
				}
			}
		}
		#pragma omp for collapse(2)
		for(int i_z=0; i_z<n_z; ++i_z) {
			for(int i_y=0; i_y<n_y; ++i_y) {
				int index = i_z*n_x*n_y+i_y*n_x+0;
				b[index] = potential_l;
				x[index] = potential_l;
				index += n_x-1;
				b[index] = potential_r;
				x[index] = potential_r;
			}
		}
		double potential_increment = (potential_r-potential_l)/(n_x-1);
		#pragma omp for collapse(2) nowait
		for(int i_z=0; i_z<n_z; ++i_z) {
			for(int i_x=0; i_x<n_x; ++i_x) {
				int index1 = i_z*n_x*n_y+0*n_x+i_x;
				int index2 = i_z*n_x*n_y+(n_y-1)*n_x+i_x;
				b[index1] = b[index2] = potential_l+potential_increment*i_x;
				x[index1] = x[index2] = potential_l+potential_increment*i_x;
			}
		}
		#pragma omp for collapse(2)
		for(int i_y=0; i_y<n_z; ++i_y) {
			for(int i_x=0; i_x<n_x; ++i_x) {
				int index1 = 0*n_x*n_y+i_y*n_x+i_x;
				int index2 = (n_z-1)*n_x*n_y+i_y*n_x+i_x;
				b[index1] = b[index2] = potential_l+potential_increment*i_x;
				x[index1] = x[index2] = potential_l+potential_increment*i_x;
			}
		}
	}

	double *temp = new double[n_x*n_y*n_z];
	#pragma omp parallel
	{
		#pragma omp for
		for(int i=0; i<n_x*n_y*n_z; ++i) {
			temp[i] = b[i];
		}
	}
	int iter=0;
	while(iter++<1000) {
		#pragma omp parallel
		{
			#pragma omp for collapse(2)
			for(int i=0; i<n_x*n_y*n_z; ++i) {
				for(int j=0; j<n_x*n_y*n_z; ++j) {
					temp[i]-=(A[i][j]*x[j]);
				}
			}
			#pragma omp for
			for(int i=0; i<n_x*n_y*n_z; ++i) {
				x[i] = temp[i]/A[i][i];
				temp[i] = b[i];
			}
		}
	}

	// deallocate
	#pragma omp parallel 
	{
		#pragma omp for
		for(int i=0; i<n_x*n_y*n_z; ++i) {
			delete [] A[i];
		}
	}
	delete [] x;
	delete [] b;

	return 0;
}